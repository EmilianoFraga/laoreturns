# VRAM ADRESSES FOR THIS SCREEN 1 MODE
| what                | start address | end address |
|---------------------|---------------|-------------|
| PATTERN BLOCK 1     | `0x0000`      | `0x07FF`    |
| PATTERN BLOCK 2     | `0x0800`      | `0x0FFF`    |
| PATTERN BLOCK 3     | `0x1000`      | `0x17FF`    |
| NAME TABLE BLOCK 1  | `0x1800`      | `0x18FF`    |
| NAME TABLE BLOCK 2  | `0x1900`      | `0x19FF`    |
| NAME TABLE BLOCK 3  | `0x1A00`      | `0x1AFF`    |
| COLOR TABLE BLOCK 1 | `0x2000`      | `0x27FF`    |
| COLOR TABLE BLOCK 2 | `0x2800`      | `0x2FFF`    |
| COLOR TABLE BLOCK 3 | `0x3000`      | `0x37FF`    |

