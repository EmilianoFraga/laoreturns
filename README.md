# Dr. Lao Returns
*A review to the original Dr. Lao article*

*https://giovannireisnunes.wordpress.com/2018/06/01/as-sete-faces-da-screen1-parte-2/*

## What
This is a MSX-BASIC program that uses the original data from Dr. Lao example.

The program presents the Screen 1 and allows the user to cycle through modes, colors, as well as Dr. Lao modes (Original or Reviewed)

## Setup
### Using MSX Pen
*https://msxpen.com/*

1. Load the project's DSK file as DRIVE B: (dsk/DrLao_Original.zip)
2. Open the .bas file and paste it in the MSX Pen's edit window 
3. Click Run. MSX Pen will reset the MSX machine and the code will execute.

## How

The program will present the Dr. Lao's example Screen, *mode A0*. (Screen 1)

The screen border will be YELLOW. This means Dr. Lao is running on ORIGINAL mode.

Then you can use the **keys**:

* **SPACE**: Swap Dr. Lao modes
  * ORIGINAL: border is YELLOW
  * REVIEW: border is MAGENTA


* **b or B**: switch block 1 (1st third) **character** filling
* **n or N**: switch block 2 (2nd third) **character** filling
* **m or M**: switch block 3 (3rd third) **character** filling


* **8**: switch block 1 (1st third) **color** filling
* **9**: switch block 2 (2nd third) **color** filling
* **0**: switch block 3 (3rd third) **color** filling


* **1**: switch to screen mode D0
* **2**: switch to screen mode D1
* **3**: switch to screen mode D2
* **4**: switch to screen mode D3


* **q or Q**: switch to screen mode C0
* **w or W**: switch to screen mode C1
* **e or E**: switch to screen mode C2
* **r or R**: switch to screen mode C3


* **a or A**: switch to screen mode B0
* **s or S**: switch to screen mode B1
* **d or D**: switch to screen mode B2
* **f or F**: switch to screen mode B3


* **z or Z**: switch to screen mode A0
* **x or X**: switch to screen mode A1
* **c or C**: switch to screen mode A2
* **v or V**: switch to screen mode A3


* **ESC**: exits to Screen 0 

----
*about **character** filling modes:*
* ***full set***: fills the block with characters from 0 to 255;
* ***single char***: fills the block with fixed character 8;

*about **color** filling modes:*
* ***mixed colors***: fills the block with mixed colors;
* ***single color***: fills the block with a fixed color set per block:
  * block 1: white on red
  * block 2: white on green
  * block 3: white on blue

## Acknowledgements
Thanks to Giovanni Reis Nunes for the brilliant post about MSX Screen 1 modes.

This whole project is a humble review to his work. All credits go to him.
